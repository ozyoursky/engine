package com.simple.PHOTOLOADER.service

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeTokenRequest
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.json.jackson2.JacksonFactory.getDefaultInstance
import com.google.api.client.util.store.FileDataStoreFactory
import com.google.api.services.drive.DriveScopes.DRIVE
import com.simple.PHOTOLOADER.model.GoogleCreadMin
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.io.File
import javax.annotation.PostConstruct

@Service
class OAuthService(
        val clientCreadMin: GoogleCreadMin,
        @Value("\${photoloader.drive.gmail}") val DRiVE_EMAIL: String
) {
    private val flow: GoogleAuthorizationCodeFlow by lazy {
        clientCreadMin.let {
            GoogleAuthorizationCodeFlow.Builder(
                    HTTP_TRANSPORT,
                    JSON_FACTORY,
                    it.clientId,
                    it.clientSecret,
                    SCOPES
            )
                    .setDataStoreFactory(CREAD_STORE_FACTORY)
                    .setAccessType("offline")
                    .setApprovalPrompt("force")
                    .build()
        }
    }

    val authUrl: String by lazy {
        flow.newAuthorizationUrl().setRedirectUri(clientCreadMin.redirectUri).build()
    }

    @PostConstruct
    fun showAuthUri() {
        println(" REDIRECT_URI: ${clientCreadMin.redirectUri}")
        println(authUrl)
    }

    fun getCredential(): Credential? = flow.loadCredential(USER_ID)

    fun refreshCredentials(authCode: String): Credential {
        val tokenResponse =
                GoogleAuthorizationCodeTokenRequest(
                        HTTP_TRANSPORT,
                        JSON_FACTORY,
                        TOKEN_SERVER_URL,
                        clientCreadMin.clientId,
                        clientCreadMin.clientSecret,
                        authCode,
                        clientCreadMin.redirectUri
                ).execute()

        if (tokenResponse.parseIdToken().payload.email != DRiVE_EMAIL)
            throw IllegalAccessException("lol")
        return flow.createAndStoreCredential(tokenResponse, USER_ID)
    }

    companion object {
        private const val TOKENS_DIRECTORY_PATH = "src/main/resources/tokens"
        const val USER_ID = "storage"
        const val TOKEN_SERVER_URL = "https://www.googleapis.com/oauth2/v4/token"
        val HTTP_TRANSPORT: NetHttpTransport = GoogleNetHttpTransport.newTrustedTransport()!!
        val JSON_FACTORY: JacksonFactory = getDefaultInstance()
        val SCOPES = setOf(DRIVE, "https://www.googleapis.com/auth/userinfo.email")
        val CREAD_STORE_FACTORY = FileDataStoreFactory(File(TOKENS_DIRECTORY_PATH))
    }
}