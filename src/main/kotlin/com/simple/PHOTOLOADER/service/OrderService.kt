package com.simple.PHOTOLOADER.service

import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.springframework.stereotype.Service
import java.io.InputStream
import java.time.Clock
import java.time.LocalDateTime.now
import java.time.format.DateTimeFormatter
import javax.servlet.http.HttpServletRequest


@Service
class OrderService(
        private val storage: PhotoloaderStorageService,
        private val mailService: MailService
) {
    fun initOrder() = mapOf(
            "storeId" to storage.createTempFolder("temp: ${nowInMinsk()}"),
            "settings" to storage.settings
    )

    fun uploadFile(istr: InputStream, orderId: String, name: String): String =
            try {
                storage.run {
                    val fileId = getNextFileId()
                    uploadFromStream(istr, orderId, name, fileId)
                            .also { println("=====================  start uploading ${name}  ========================") }
                    fileId
                }
            } catch (e: Exception) {
                mailService.sendErrorMessage(e.toHumanMessage())
                throw e
            }

    fun deleteFile(orderId: String, fileId: String) = // NOTE: not used by frontend
            storage.deleteById(fileId, orderId)

    fun submitOrder(orderId: String,
                    info: Map<String, String>,
                    region: String
    ) = try {
        storage.confirmOrder(
                orderId,
                info.toOrderLetter(),
                "order: ${nowInMinsk()}",
                region
        )
    } catch (e: Exception) {
        mailService.sendErrorMessage(e.toHumanMessage())
        throw e
    }

    private fun Map<String, String>.toOrderLetter(): String = StringBuilder().apply {
        entries.forEach {
            append("${it.key}: ${it.value}\n")
        }
    }.toString()

    fun getSettings() = storage.settings
}

fun nowInMinsk(): String = now(Clock.systemUTC()).plusHours(3).format(DateTimeFormatter.ofPattern("d MMM HH:mm"))