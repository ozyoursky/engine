package com.simple.PHOTOLOADER.service

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import com.simple.PHOTOLOADER.model.settingsDto.FormSettings
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload
import org.springframework.beans.factory.annotation.Value
import org.springframework.scheduling.annotation.Async
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ConcurrentHashMap
import javax.servlet.http.HttpServletRequest

@Service
class PhotoloaderStorageService(
        private val driveService: GoogleDriveService,
        @Value("\${photoloader.folder.root:PHOTOLOADER-dev}") private val rootFolderName: String,
        @Value("\${photoloader.folder.temp:TEMP}") private val tempFolderName: String,
        @Value("\${photoloader.file.settings:SETTINGS.json}") private val settingsFileName: String,
        @Value("\${photoloader.regions}") val regions: List<String>
) {
    private val uploadedFiles = ConcurrentHashMap<String, CompletableFuture<Unit>>()

    private val mapper = jacksonObjectMapper()

    val settings: FormSettings
        get() {
            return driveService.run {
                mapper.readValue(
                        getStreamFromFile(
                                getFileIdByNameAndFolderId(settingsFileName, rootFolderId),
                                ByteArrayOutputStream()
                        ).toString("UTF-8")
                )
            }
        }

    private val regionFolders: Map<String, String> by lazy {
        regions.map { regionName ->
            regionName to createFolderIfNotExist(regionName)
        }.toMap()
    }

    private val rootFolderId: String by lazy {
        getFolderIdByName(rootFolderName)
                ?: driveService.createFolder(rootFolderName)
    }

    private val rootTempFolderId: String by lazy {
        createFolderIfNotExist(tempFolderName)
    }

    fun getNextFileId(): String = driveService.nextFileId()

    @Async
    fun uploadFromStream(
            iStr: InputStream,
            orderId: String? = rootFolderId,
            filename: String?,
            fileId: String
    ) {
        val fut = CompletableFuture<Unit>()
        uploadedFiles[fileId] = fut
        inFuture(fut) {
            driveService.uploadFromStream(iStr, orderId, filename, fileId)
        }
        uploadedFiles.remove(fileId)
    }

    @Async
    fun deleteById(
            fileId: String,
            orderId: String
    ) {
        uploadedFiles[fileId]?.join()
        driveService.deleteById(fileId, orderId).also {
            println("============  $fileId deleted from folder $orderId   ============")
        }
    }

    @Async
    fun confirmOrder(orderId: String,
                     orderText: String,
                     orderName: String,
                     region: String
    ) {
        driveService.run {
            uploadFromStream(
                    orderText.byteInputStream(),
                    orderId,
                    "$orderName.txt"
            )
            renameFolder(orderId, orderName)
            changeParent(orderId, rootTempFolderId, region.toRegionFolderId())
        }
    }

    fun createTempFolder(folderName: String): String =
            driveService.createFolder(folderName, rootTempFolderId)

    private fun String.toRegionFolderId() =
            regionFolders[this] ?: (regionFolders[DEFAULT_REGION]
                    ?: error("not found DEFAULT_REGION = $DEFAULT_REGION"))

    private final fun createFolderIfNotExist(folderName: String) =
            getFolderIdByNameAndParentId(folderName, rootFolderId)
                    ?: driveService.createFolder(folderName, rootFolderId)

    private fun getFileIdByNameAndFolderId(fileName: String, folderId: String) = driveService
            .getContentByQuery("name='$fileName' and '$folderId' in parents")
            ?.get(0)?.id

    private fun getFolderIdByName(fileName: String) = driveService
            .getContentByQuery("mimeType='application/vnd.google-apps.folder' and name='$fileName'")
            ?.get(0)?.id

    private fun getFolderIdByNameAndParentId(fileName: String, parentId: String?) = driveService
            .getContentByQuery("mimeType='application/vnd.google-apps.folder' and name='$fileName' and '$parentId' in parents ")
            ?.get(0)?.id

    private fun inFuture(fut: CompletableFuture<Unit>, function: () -> Unit) {
        function()
        fut.complete(Unit)
    }

    @Scheduled(
            fixedDelayString = "\${photoloader.scheduler.delay:60000}",
            initialDelayString = "\${photoloader.scheduler.delay:60000}" // in milliseconds
    )
    fun cleanDrive() {
        deleteExpireFolders()
        driveService.emptyTrash()
    }

    private fun deleteExpireFolders() = driveService.run {
        getContentByQuery(
                EXPIRE_FOLDER_QUERY.format(
                        rootTempFolderId,
                        nowInGoogleFormat().minusMinutes(settings.session.destroyTimeoutMin)
                ))
                ?.map { it.id }
                ?.forEach { deleteById(it) }
    }

    companion object {
        private const val DEFAULT_REGION = "BY"
        private const val EXPIRE_FOLDER_QUERY = "mimeType='application/vnd.google-apps.folder' and '%s' in parents and modifiedTime < '%s'"
    }
}