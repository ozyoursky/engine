package com.simple.PHOTOLOADER.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.mail.SimpleMailMessage
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.stereotype.Service

@Service
class MailService {
    @Value("\${spring.mail.username}")
    private lateinit var appEmail: String

    @Value("\${photoloader.admin.email}")
    private lateinit var adminEmail: String

    @Autowired
    private lateinit var mailSender: JavaMailSender

    private val preparedMessage: SimpleMailMessage
        get() {
            return SimpleMailMessage().apply {
                setFrom(appEmail)
                setTo(adminEmail)
                setSubject("PRINT-PHOTO")
            }
        }

    fun sendErrorMessage(message: String) {
        mailSender.send(preparedMessage.withText(message))
    }

    fun SimpleMailMessage.withText(text: String) = apply { setText(text) }
}