package com.simple.PHOTOLOADER.service

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.googleapis.json.GoogleJsonResponseException
import com.google.api.client.http.AbstractInputStreamContent
import com.google.api.client.http.InputStreamContent
import com.google.api.client.util.DateTime
import com.google.api.services.drive.Drive
import com.simple.PHOTOLOADER.service.OAuthService.Companion.HTTP_TRANSPORT
import com.simple.PHOTOLOADER.service.OAuthService.Companion.JSON_FACTORY
import org.springframework.stereotype.Service
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.time.Clock
import java.time.LocalDateTime
import java.time.LocalDateTime.now
import java.util.*
import java.util.concurrent.ConcurrentLinkedQueue
import com.google.api.services.drive.model.File as DriveFile

@Service
class GoogleDriveService(
        private val authService: OAuthService
) {
    private val allowedIds = ConcurrentLinkedQueue<String>()

    private val driveClient: Drive by lazy {
        if (authService.getCredential() == null)
            throw IllegalAccessException("lol")
        getNewClient()
    }

    private fun getNewClient() = Drive.Builder(
            HTTP_TRANSPORT,
            JSON_FACTORY,
            authService.getCredential()
    )
            .setApplicationName("app")
            .build()

    private fun generateIds() {
        allowedIds.addAll(
                driveClient.files()
                        .generateIds().setCount(100).execute().ids
        )
    }

    private fun uploadContent(content: AbstractInputStreamContent, parentId: String?, fileName: String?, fileId: String?) =
            driveClient.files()
                    .create(
                            DriveFile().apply {
                                fileId?.let { id = it }
                                name = fileName
                                parents = listOf(parentId)
                            },
                            content
                    )
                    .execute()
                    .also {
                        println(" $fileName created")
                        parentId?.let { applyModifiedTime(it) }
                    }

    fun nextFileId(): String =
            allowedIds.run {
                if (isEmpty())
                    generateIds()
                poll()
            }

    fun getContentByQuery(query: String): List<DriveFile>? {
        return try {
            val files =
                    driveClient.files().list()
                            .setQ(query)
                            .setSpaces("drive")
                            .setFields("files(id)")
                            .execute()
                            .files
            if (files.isEmpty()) null
            else files
        } catch (e: GoogleJsonResponseException) {
            null
        }
    }

    fun getStreamFromFile(fileId: String?, outputStream: ByteArrayOutputStream) =
            outputStream.apply {
                driveClient.files()
                        .get(fileId)
                        .executeMediaAndDownloadTo(this)
            }

    fun uploadFromStream(iStream: InputStream, folderId: String?, fileName: String?, fileId: String? = null) {
        iStream.use { istr ->
            uploadContent(InputStreamContent(null, istr), folderId, fileName, fileId)  //type is autoresolved
        }
    }

    fun deleteById(id: String?, parentId: String? = null) {
        driveClient.files()
                .delete(id)
                .execute()
                .also { parentId?.let { applyModifiedTime(it) } }
    }

    fun createFolder(folderName: String, parentId: String? = null): String =
            driveClient.files()
                    .create(DriveFile().apply {
                        name = folderName
                        mimeType = "application/vnd.google-apps.folder"
                        parentId?.let { parents = listOf(parentId) }
                    })
                    .setFields("id")
                    .execute()
                    .id

    fun renameFolder(id: String, newName: String) {
        driveClient.files()
                .update(id, DriveFile().apply { name = newName })
                .execute()
    }

    fun emptyTrash() {
        driveClient.files()
                .emptyTrash()
                .execute()
    }

    fun changeParent(id: String, oldParentId: String, newParentId: String) {
        driveClient.files()
                .update(id, DriveFile())
                .apply {
                    removeParents = oldParentId
                    addParents = newParentId
                }
                .execute()
    }

    fun applyModifiedTime(id: String) {
        driveClient.files()
                .update(id, DriveFile().apply { modifiedTime = DateTime(Date().time) })
                .execute()
    }

    fun refreshCredentials() = (driveClient.requestFactory.initializer as Credential).run {
        authService.getCredential()?.let {
            setAccessToken(it.accessToken)
            setRefreshToken(it.refreshToken)
        } ?: throw IllegalAccessException("lol")
    }
}

fun nowInGoogleFormat(): LocalDateTime = now(Clock.systemUTC())

fun Exception.toHumanMessage() = "${this.message} \n ${this.cause} \n ${this.stackTrace}"