package com.simple.PHOTOLOADER

import org.springframework.boot.Banner.Mode.OFF
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.annotation.EnableScheduling

@EnableAsync
@EnableScheduling
@SpringBootApplication
class Starter

fun main(args: Array<String>) {
    runApplication<Starter>(*args) { setBannerMode(OFF) }
}