package com.simple.PHOTOLOADER.controller

import com.simple.PHOTOLOADER.service.OrderService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.ui.set
import org.springframework.web.bind.annotation.GetMapping

@Controller  // TODO must be removed after merge with frontEnd
class InitialController {
    @Autowired
    private lateinit var orderService: OrderService

    @GetMapping("/main")
    fun getPage(model: Model): String {
        val initialId = orderService.initOrder()
        model["id"] = initialId
        return "initialPage"
    }
}