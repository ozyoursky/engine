package com.simple.PHOTOLOADER.controller

import com.simple.PHOTOLOADER.service.OrderService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@CrossOrigin
@RestController
@RequestMapping("/")
class OrderController {
    @Autowired
    private lateinit var orderService: OrderService

    @GetMapping("/init")
    fun createFolder() = orderService.initOrder()

    @GetMapping("/settings")
    fun getRegions() = orderService.getSettings()

    @PostMapping("/{id}/file")
    fun uploadFile(
            @PathVariable id: String,
            @RequestParam file: MultipartFile
    ) = orderService.uploadFile(file.inputStream, id, file.originalFilename!!)

    @DeleteMapping("/{id}/file/{fileId}")
    fun deleteFile(
            @PathVariable id: String,
            @PathVariable fileId: String
    ) = orderService.deleteFile(id, fileId)

    @PostMapping("/{id}/submit")
    fun submitOrder(
            @PathVariable id: String,
            @RequestParam("region", defaultValue = "") region: String,
            @RequestBody info: Map<String,String>
    ) = orderService.submitOrder(id, info, region)
}

