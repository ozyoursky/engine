package com.simple.PHOTOLOADER.controller

import com.simple.PHOTOLOADER.service.GoogleDriveService
import com.simple.PHOTOLOADER.service.OAuthService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/photoloader/code")
class OAuthController {
    @Autowired
    private lateinit var authService: OAuthService

    @Autowired
    private lateinit var driveService: GoogleDriveService

    @GetMapping
    fun getCode(@RequestParam("code") code: String) {
        authService.refreshCredentials(code)
        driveService.refreshCredentials()
    }
}