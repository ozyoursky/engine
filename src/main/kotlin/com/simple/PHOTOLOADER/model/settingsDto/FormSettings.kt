package com.simple.PHOTOLOADER.model.settingsDto

data class FormSettings(
        val printTypes: List<Any>,
        val regions: List<Any>,
        val session: FormSession
)