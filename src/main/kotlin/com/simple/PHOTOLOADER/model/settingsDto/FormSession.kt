package com.simple.PHOTOLOADER.model.settingsDto

data class FormSession(
        val warningTimeoutMin: Long,
        val destroyTimeoutMin: Long
)