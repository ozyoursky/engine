package com.simple.PHOTOLOADER.model

import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

@Component
class GoogleCreadMin(
        @Value("\${google.oauth.client_id:ooo}")
        val clientId: String,

        @Value("\${google.oauth.client_secret:ooo}")
        val clientSecret: String,

        @Value("\${google.oauth.redirect_uri:ooo}")
        val redirectUri: String
)
