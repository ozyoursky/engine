package com.simple.PHOTOLOADER.model

class SubmitInfo(
        val formData: Map<String, String>,
        val removedFiles: List<String>
)