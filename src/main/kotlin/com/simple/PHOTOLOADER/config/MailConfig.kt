package com.simple.PHOTOLOADER.config

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.JavaMailSenderImpl

@Configuration
class MailConfig {
    @Value("\${spring.mail.password}")
    private lateinit var pwd: String

    @Value("\${spring.mail.username}")
    private lateinit var login: String

    @Value("\${spring.mail.host}")
    private lateinit var mailHost: String

    @Value("\${spring.mail.port:465}")
    private var mailPort: Int = 0

    @Value("\${mail.protocol}")
    private lateinit var mailProtocol: String

    @Value("\${mail.debug}")
    private lateinit var debugFlag: String

    @Bean
    fun getJavaMailSender(): JavaMailSender = JavaMailSenderImpl().apply {
            host = mailHost
            port = mailPort
            username = login
            password = pwd
        }.also {
            it.javaMailProperties.apply {
                setProperty("mail.transport.protocol", mailProtocol)
                setProperty("mail.debug", debugFlag)
            }
        }
}