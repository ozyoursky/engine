package com.simple.PHOTOLOADER.config

import org.springframework.stereotype.Component
import org.springframework.web.filter.GenericFilterBean
import javax.servlet.FilterChain
import javax.servlet.ServletRequest
import javax.servlet.ServletResponse
import javax.servlet.http.HttpServletResponse

@Component
class ResponseFilter : GenericFilterBean() {
    override fun doFilter(rq: ServletRequest?, response: ServletResponse?, chain: FilterChain) {
        (response as HttpServletResponse).apply {
            addHeader("Access-Control-Allow-Origin", "*")
            addHeader("Access-Control-Allow-Methods", "GET, POST, DELETE, OPTIONS")
            addHeader("Access-Control-Allow-Headers", "Content-Type")
        }
        chain.doFilter(rq, response)
    }
}