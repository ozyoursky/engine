<html>
<head>
    <meta charset="UTF-8">
    <title>photoloader</title>
</head>
<body>
<div id="app">
    <div class="Loader">
        <input type="file" @change="onChanged" title="vue loader">
        <button @click="onUpload">Upload axios!</button>
        <button @click="onDelete">DELETE!</button>
        <button @click="onSubmit">Submit</button>
    </div>
</div>
${id}

<script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script>
    var id = "${id}"
    var vm = new Vue({
        el: '#app',
        data: {
            selectedFile: null
        },
        methods: {
            onChanged(event) {
                console.log(event)
                this.selectedFile = event.target.files[0]
            },
            onUpload(event) {
                const formData = new FormData()
                formData.append('file', this.selectedFile)
                console.log(formData)
                axios.post('/'+id+"/file", formData)
                console.log("creatre " + this.selectedFile.name)
            },
            onDelete(event) {
                axios.delete('/'+id+'/file/' + this.selectedFile.name)
            },
            onSubmit(event) {
                const formData = new FormData()
                myinfo = new Map()
                myinfo.set("name", "some name")
                myinfo.set("order", "some order")
                axios.post("/"+id+"/submit" ,
                    {
                        "name": "some name",
                        "order": "some order"
                    })
            }
        }
    })
</script>
</body>
</html>