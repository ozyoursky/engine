if [ "$1" = "f" ]; then
#  cd ~/IdeaProjects/photo-loader/
   cd ~/IdeaProjects/vue-test/vue-test/
   npm run build
  cp -r dist/. ~/IdeaProjects/p-l-k/src/main/resources/static/
fi

cd ~/IdeaProjects/p-l-k/
mvn clean package

echo "######  deploy to DEV  #######"
scp -i "$PATH_TO_INSTACNE_KEY" target/PHOTOLOADER-0.0.1-SNAPSHOT.jar ec2-user@ec2-3-121-215-13.eu-central-1.compute.amazonaws.com:~
ssh -i "$PATH_TO_INSTACNE_KEY" ec2-user@ec2-3-121-215-13.eu-central-1.compute.amazonaws.com 'sudo systemctl restart photoloader-server'